#!/usr/bin/env bash
# Script for download languages for botpress NLU server

if [ -z "$1" ]; then
  echo 'Please pass language code. E.g. `langdl.sh en`'
  exit 0
fi

LANGUAGE=$1
echo ${LANGUAGE}

wget https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.${LANGUAGE}.300.bin.gz
gunzip cc.${LANGUAGE}.300.bin.gz
mv cc.${LANGUAGE}.300.bin bp.${LANGUAGE}.300.bin

wget https://bpemb.h-its.org/${LANGUAGE}/${LANGUAGE}.wiki.bpe.vs3000.model
mv ${LANGUAGE}.wiki.bpe.vs3000.model bp.${LANGUAGE}.bpe.model

echo "Finished downloading language=${LANGUAGE}"

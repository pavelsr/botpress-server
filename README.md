# What is it ?

Set of configs for quick start botpress server

# Pre-setup

You will need to install language model files for NLU server using `langdl.sh`

If you want to make language download script easily accessed from any folder just go to `/usr/local/bin` and make a symlink to `langdl.sh`

or directly download it to /usr/local/bin :

```
sudo curl -L https://gitlab.com/pavelsr/botpress-server/-/raw/master/langdl.sh -o /usr/local/bin/bplangdl && sudo chmod +x /usr/local/bin/bplangdl
```

If you use `local-nodb` setup you may need to create `data` and `language` dirs :


```
sudo mkdir -m 777 data language
```

# Post-setup

After git pull it's recommended to do:

```
git reset --hard
cp --remove-destination `readlink docker-compose.yaml` docker-compose.yaml
```

# Setup into existing folder

```
git init
git remote add origin git@gitlab.com:pavelsr/botpress-server.git
git fetch origin
git checkout -b master --track origin/master
```


# Setup without git

If you don't want to download whole repository (or if you can't - if can't have git installed) - you can setup like this:


```
target=local
curl https://gitlab.com/pavelsr/botpress-server/-/raw/master/${target}/.env.example -o .env.env.example
curl https://gitlab.com/pavelsr/botpress-server/-/raw/master/${target}/docker-compose.yaml -o docker-compose.yaml
# curl https://gitlab.com/pavelsr/botpress-server/-/raw/master/${target}/docker-compose.override.yaml -o docker-compose.override.yaml
cp .env.example .env
# nano .env
# docker volume create pgdata
docker-compose up
```
